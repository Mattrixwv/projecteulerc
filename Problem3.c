//ProjectEuler/C/Problem3.c
//Matthew Ellison
// Created: 03-11-19
//Modified: 03-28-19
//The largest prime factor of 600851475143
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/myHelpers
//This program need the -lm flag to compile
/*
	Copyright (C) 2019  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


#include <stdio.h>
#include <inttypes.h>
#include "Stopwatch.h"
#include "DynamicInt64Array.h"
#include "Algorithms.h"

const int64_t GOAL_NUMBER = 600851475143;	//The number you are trying to find the factors of


int main(){
	//Setup your variables
	struct Stopwatch timer;	//Used to determine your algorithm's run time
	initStopwatch(&timer);

	//Start the timer
	startStopwatch(&timer);

	//Find the factors of GOAL_NUMBER
	struct DynamicInt64Array factors = getFactors(GOAL_NUMBER);	//Holds the factors of goalNumber
	//Stop the timer
	stopStopwatch(&timer);
	char* timerStr = getStrStopwatch(&timer);

	//Print the results
	printf("The largest factor of the number %lu is %lu\n", GOAL_NUMBER, getDynamicInt64Array(&factors, factors.size - 1));
	printf("It took %s to run this algorithm\n", timerStr);

	//Free the memory used by the string
	destroyDynamicInt64Array(&factors);
	free(timerStr);
	timerStr = NULL;

	return 0;
}

/* Results:
The largest factor of the number 600851475143 is 6857
It took 1.297 seconds to run this algorithm
*/
