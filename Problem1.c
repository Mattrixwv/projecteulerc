//ProjectEuler/C/Problem1.c
//Matthew Ellison
// Created: 03-08-19
//Modified: 03-28-19
//What is the sum of all the multiples of 3 or 5 that are less than 1000
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/myHelpers
/*
	Copyright (C) 2019  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


#include <stdio.h>
#include <inttypes.h>
#include "Stopwatch.h"

const int MAX_NUMBER = 1000;


int main(){
	//Setup the stopwatch
	struct Stopwatch timer;
	initStopwatch(&timer);
	//Setup the variables you will need

	uint64_t fullSum = 0;	//For the sum of all the numbers

	//Start the timer
	startStopwatch(&timer);

	//Step through ever number < 1000 and see if either 3 or 5 divides it evenly
	for(uint64_t cnt = 1;cnt < MAX_NUMBER;++cnt){
		if((cnt % 3) == 0){
			fullSum += cnt;
		}
		else if((cnt % 5) == 0){
			fullSum += cnt;
		}
	}

	//Stop the timer
	stopStopwatch(&timer);
	char* timerStr = getStrStopwatch(&timer);

	//Print the results
	printf("The sum of all the numbers < %d that are divisible by 3 or 5 is %lu\n", MAX_NUMBER, fullSum);
	printf("It took %s to run this algorithm\n", timerStr);

	//Free the memory taken up by the string
	free(timerStr);
	timerStr = NULL;

	return 0;
}

/* Results:
The sum of all the numbers < 1000 that are divisible by 3 or 5 is 233168
It took 3.000 microseconds to run this algorithm
*/
